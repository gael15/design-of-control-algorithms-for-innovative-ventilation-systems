#README

## Name of project
Design of control algorithms for innovative ventilation systems.

## Description
The aim is to design a control algorithm to defrost the heat exchanger of the dual flow ventilation system in order to reduce the energy consumption of the system.

## Installation
Ecosystem : Matlab/simulink is used to do this project.

## Usage
Defrosting of the dual flow VMC heat exchanger.



