%%%% Generation du scenario d'humidité relative intérieure %%%
%%%% Fait le 02/08/2022
%%% Données temperatures froides
%%%vectTemp3 : Temperature Karasjok
%%%vectTemp2 : Temperature Helsinki
load DAH_V1.1.mat
load DonneeMeteo.mat
Hr_vect_scen = zeros(size(vectTemp3));
PdR = zeros(size(vectTemp3));
for idx = 1 : length(vectTemp3)
    [~, PE, ~, ~,~] = Psychrometricsnew('Tdb',vectTemp3(idx),'phi',100 );
    [~, ~, Hr_vect_scen(idx), ~,~] = Psychrometricsnew('Tdb',18,'w', PE);
    Hr_vect_scen(idx) = max(min(Hr_vect_scen(idx),90),5);
    %PdR(idx) = interp2(DAH_vect_T,DAH_vect_Hr,DAH',18,Hr_vect_scen(idx));
end
t=1:1:length(Hr_vect_scen);
t=t';
Cs_hr_extraite = [t*3600,Hr_vect_scen];%% Hr extraite modelisée                                                                     