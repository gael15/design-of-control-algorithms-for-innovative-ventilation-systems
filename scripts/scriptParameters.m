%%%% Datas système de dégivrage%%%
%%%% fait le : 02/07/2022
%%Définition des paramètres%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Bs_dem_sys_C:Booleen scalaire pour activation du dégivrage
%Bs_fnc_degivrage_C:Booleen scalaire pour fonctionnement avec ou sans BCA
%Ns_temp_ext_C: Condition de temperature extérieure avec BCA
%Ns_temp_ext_sans_BCA_C:Condition de temperature extérieure sans BCA
%Ns_temp_exhaust_max_C:Condition de température rejetée maximale
%Ns_temp_exhaust_min_C:Condition de température rejetée minimale
%Ns_temp_vmc_C:Temperature extraite par la VMC
%Ns_cons_bca_C:Entier scalaire designant consigne de température BCA 
%Ns_val_tol_cns:Entier scalaire designant la tolérance de consigne BCA
%Ns_timer_full_bat:Entier scalaire designant la durée du mode BCA et mode Safe
%Ns_timer : Entier scalaire designant la durée de verification des conditions
%Ns_timer_degivr:Timer d'arret pour degivrage
%Bs_passivhauss:Booleen scalaire pour mode passivhauss
%Bs_no_passivhauss:Booleen scalaire pour mode non passivhauss
%Ns_cdn_freeze_point:Entier scalaire pour determiner le risque de givre
%Ns_val_tol_hr:Entier scalaire désignant tolérance sur valeur Hr extraite
%Ns_power_bca_max_C:Entier scalaire désignant la puissance max de la BCA en KW
%Ns_power_bca_min_C:Entier scalaire désignant la puissance min de la BCA en KW
%Ns_debit_max_C:Entier scalaire désignant le debit maximal en m^3/h
%Ns_debit_quot_C:Entier scalaire désignant le debit quotidien en m^3/h
%Ns_debit_min_C:Entier scalaire désignant le debit minimal en m^3/h
%Ns_bca_mode_C:Entier scalaire désignant le Mode 1 du dégivrage
%Ns_safe_mode_C:Entier scalaire désignant le Mode 2 du dégivrage
%Ns_freeze_mode_C:Entier scalaire désignant le Mode 3 du dégivrage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Pramètres%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Bs_dem_sys_C = 1;
Bs_fnc_degivrage_C = 1;
Ns_temp_ext_C = -3;
Ns_temp_ext_sans_BCA_C = -3.5; 
Ns_temp_exhaust_max_C = 14;
Ns_temp_exhaust_min_C = 4;
Ns_temp_vmc_C = 18;
Ns_cons_bca_C = -3; 
Ns_val_tol_cns_C = 3;
Ns_timer_full_bat = 600;
Ns_timer = 900;
Ns_timer_degivr = 7200;
Bs_passivhauss = 1;
Bs_no_passivhauss = 0;
Ns_cdn_freeze_point = 0;
Ns_chal_volumique_C = 0.36;
Ns_val_tol_hr = 5;
Ns_power_bca_max_C = 1.5; 
Ns_power_bca_min_C = 0;
Ns_debit_max_C = 200;
Ns_debit_quot_C = 100 ;
Ns_debit_min_C = 5 ;
Ns_fnc_normal_C = 0;
Ns_bca_mode_C = 1;
Ns_safe_mode_C = 2;
Ns_freeze_mode_C = 3;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% end %%%

%%%Sauvegarde des simulations
%%%%Scénarios d'humidités relatives
%save simulhr.mat out;
%save simulhr2.mat out;
%save simulhr3.mat out;
%%%% 1er scenario de temperature : Karasjok
%save simul_existing_vacant_house.mat out;
%save simul_existing_low_occupancy_house.mat out;
%save simul_existing_occupied_house.mat out;
%save simul_Hr_vacant_house.mat out;
%save simul_Hr_low_occupancy_house.mat out;
%save simul_Hr_occupied_house.mat out;
%%%% 2ème scenario de temperature: Helsinki
%save simul2_existing_vacant_house.mat out;
%save simul2_existing_low_occupancy_house.mat out;
%save simul2_existing_occupied_house.mat out;
%save simul2_Hr_vacant_house.mat out;
%save simul2_Hr_low_occupancy_house.mat out;
% save simul2_Hr_occupied_house.mat out;
