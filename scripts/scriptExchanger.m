%%%% Exchanger script%%%
%%%% Fait le 03/06/2022
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load rawDataExtract.mat 
% 
% Vect_HrInt = ... 
%     unique(HrInt.VarName3(nonzeros((~isnan(HrInt.VarName3)).*[1:length(HrInt.VarName3)]'))) ; 
% 
% Vect_TempInt = ... 
%     unique(TempInt.VarName2(nonzeros((~isnan(TempInt.VarName2)).*[1:length(TempInt.VarName2)]'))) ; 
% 
% Vect_HrExt = ... 
%     unique(HrExt.VarName5(nonzeros((~isnan(HrExt.VarName5)).*[1:length(HrExt.VarName5)]'))) ; 
% 
% Vect_TempExt = ... 
%     unique(TempExt.VarName4(nonzeros((~isnan(TempExt.VarName4)).*[1:length(TempExt.VarName4)]'))) ;
% 
% Vect_Q = ... 
%     unique(Q.VarName6(nonzeros((~isnan(Q.VarName6)).*[1:length(Q.VarName6)]'))) ;

Map_efficacite = zeros(length(Vect_Q),length(Vect_TempExt),length(Vect_HrInt),length(Vect_HrExt)); 
Map_PressDrop =  zeros(length(Vect_Q),length(Vect_TempExt),length(Vect_HrInt),length(Vect_HrExt));
Map_HeatTRansfer = zeros(length(Vect_Q),length(Vect_TempExt),length(Vect_HrInt),length(Vect_HrExt));
Map_Condens = zeros(length(Vect_Q),length(Vect_TempExt),length(Vect_HrInt),length(Vect_HrExt));
Map_TempSouffle = zeros(length(Vect_Q),length(Vect_TempExt),length(Vect_HrInt),length(Vect_HrExt)); 
Map_TempExtrait = zeros(length(Vect_Q),length(Vect_TempExt),length(Vect_HrInt),length(Vect_HrExt)); 
Map_HrSouffle = zeros(length(Vect_Q),length(Vect_TempExt),length(Vect_HrInt),length(Vect_HrExt));
Map_HrExtrait = zeros(length(Vect_Q),length(Vect_TempExt),length(Vect_HrInt),length(Vect_HrExt));

lastValue_HrExt = 0 ; 
lastValue_PressDrop = 0 ;         
lastValue_HeatTrasfer = 0 ; 
lastValue_Condens = 0 ; 
lastValue_TempSouffle = 0; 
lastValue_TempExtrait = 0 ; 
lastValue_HrSouffle = 0 ; 
lastValue_HrExtrait = 0 ; 

for idx_Q = 1 : length(Vect_Q)
%     for idx_tempInt = 1 : length(Vect_TempInt)
        for idx_tempExt = 1 : length(Vect_TempExt)
            for idx_HrInt = 1 : length(Vect_HrInt)
                for idx_HrExt = 1 : length(Vect_HrExt)
                
                [~,detect_Q] = ismember(Q.VarName6,Vect_Q(idx_Q));
%                 [~,detect_TempInt] = ismember(TempInt.VarName2,Vect_TempInt(idx_tempInt));
                [~,detect_TempInt] = ismember(TempInt.VarName2,16);
                [~,detect_HrInt]  = ismember(HrInt.VarName3,Vect_HrInt(idx_HrInt)); 
                [~,detect_TempExt] = ismember(TempExt.VarName4,Vect_TempExt(idx_tempExt)); 
                [~,detect_HrExt]  = ismember(HrExt.VarName5,Vect_HrExt(idx_HrExt)) ; 

                Loc_Q = nonzeros(detect_Q.*([1:length(Q.VarName6)]'));
                Loc_TempInt = nonzeros(detect_TempInt.*([1:length(TempInt.VarName2)]'));
                Loc_HrInt = nonzeros(detect_HrInt.*([1:length(HrInt.VarName3)]'));
                Loc_TempExt = nonzeros(detect_TempExt.*([1:length(TempExt.VarName4)]'));
                Loc_HrExt = nonzeros(detect_HrExt.*([1:length(HrExt.VarName5)]'));

                idxRawData  = intersect(Loc_Q,intersect(Loc_TempInt,intersect(Loc_HrInt,intersect(Loc_TempExt,Loc_HrExt)))) ; 

                if isempty(idxRawData)
                    disp('Empty Data')
                end

                if isnan(effic.efficacit(idxRawData))
                    Map_efficacite(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt) = lastValue_HrExt ; 
                else
                    Map_efficacite(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt) = effic.efficacit(idxRawData); 
                    lastValue_HrExt = effic.efficacit(idxRawData); 
                end

                if isnan(PressDrop.chuteDePressionPa(idxRawData))
                    Map_PressDrop(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt) = lastValue_PressDrop ; 
                else
                    Map_PressDrop(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt) = PressDrop.chuteDePressionPa(idxRawData);  
                    lastValue_PressDrop = PressDrop.chuteDePressionPa(idxRawData); 
                end

                
                if isnan(HeatTrans.chaleurTransfreW(idxRawData))
                    Map_HeatTRansfer(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt) = lastValue_HeatTrasfer ; 
                else
                    Map_HeatTRansfer(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt) = HeatTrans.chaleurTransfreW(idxRawData); 
                    lastValue_HeatTrasfer = HeatTrans.chaleurTransfreW(idxRawData);
                end
                 
                if isnan(Condens.condensationkgh(idxRawData))
                    Map_Condens(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt)  = lastValue_Condens ; 
                else
                    Map_Condens(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt) = Condens.condensationkgh(idxRawData); 
                    lastValue_Condens = Condens.condensationkgh(idxRawData);
                end
                
                if isnan(Temps.tempratureC(idxRawData+3))
                    Map_TempSouffle(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt) = lastValue_TempSouffle ; 
                else
                    Map_TempSouffle(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt) = Temps.tempratureC(idxRawData+3) ; 
                    lastValue_TempSouffle = Temps.tempratureC(idxRawData+3);
                end
                
                if isnan(Temps.tempratureC(idxRawData+4))
                    Map_TempExtrait(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt) = lastValue_TempExtrait; 
                else
                    Map_TempExtrait(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt) = Temps.tempratureC(idxRawData+4) ; 
                    lastValue_TempExtrait = Temps.tempratureC(idxRawData+4);
                end
                
                if isnan(Hrs.humiditRelative(idxRawData+3))
                    Map_HrSouffle(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt) = lastValue_HrSouffle; 
                else
                    Map_HrSouffle(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt) = Hrs.humiditRelative(idxRawData+3) ;  
                    lastValue_HrSouffle = Hrs.humiditRelative(idxRawData+3);
                end
                
                if isnan(Hrs.humiditRelative(idxRawData+4))
                    Map_HrExtrait(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt) = lastValue_HrExtrait; 
                else
                    Map_HrExtrait(idx_Q,idx_tempExt,idx_HrInt,idx_HrExt) = Hrs.humiditRelative(idxRawData+4) ;
                    lastValue_HrExtrait = Hrs.humiditRelative(idxRawData+4);
                end
                 


                end
%             end
        end
    end
end
