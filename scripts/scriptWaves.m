%%%% Datas waves in hours%%%
%%%% Fait le 24/06/2022
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Uploading of datas
%%% First scenario of outdoor temperature : Karasjok 
load('simul_existing_vacant_house.mat')
out1 = out;
load('simul_existing_low_occupancy_house.mat')
out2 = out;
load('simul_existing_occupied_house.mat')
out3 = out;
load('simul_Hr_vacant_house.mat')
out4 = out;
load('simul_Hr_low_occupancy_house.mat')
out5 = out;
load('simul_Hr_occupied_house.mat')
out6 = out;

%%% Second scenario of outdoor temperature : Helsinki
load('simul2_existing_vacant_house.mat')
out7 = out;
load('simul2_existing_low_occupancy_house.mat')
out8 = out;
load('simul2_existing_occupied_house.mat')
out9 = out;
load('simul2_Hr_vacant_house.mat')
out10 = out;
load('simul2_Hr_low_occupancy_house.mat')
out11 = out;
load('simul2_Hr_occupied_house.mat')
out12 = out;
load('simulhr1.mat')
out13 = out;
load('simulhr2.mat')
out14 = out;
load('simulhr3.mat')
out15 = out;
% %%%Time real datas meteo 
time = 0:1/3600:259200/3600;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%values of times for scenario modelelised
figure(1);
subplot(2,3,1);
hold on;
plot(time,out1.Temp_Air_Ext,'',linewidth =2);
plot(time,out1.Temp_Air_chauffe,'m',linewidth =2);
plot(time,out4.Temp_Air_chauffe,'k',linewidth =2);
plot(time,out5.Temp_Air_chauffe,'r',linewidth =2);
plot(time,out6.Temp_Air_chauffe,'b',linewidth =2);
ylabel('Temperature(°C)');
xlabel('Time(hours)');
legend('Temp-Fresh-Air','Temp-Heated-Air-exist-sys-for-any-Hr','Temp-Heated-Air-with-Hr-vacant-house','Temp-Heated-Air-with-Hr-low-occupancy-house','Temp-Heated-Air-with-Hr-occupied-house');
grid on;
hold off;
subplot(2,3,2);
title('Temperatures for real outdoor temperature scenario city A','FontSize',16,'FontName','Times New Roman','Color','k');
hold on;
plot(time,out1.Temp_Air_Souffle,'m',linewidth =2);
plot(time,out4.Temp_Air_Souffle,'k',linewidth =2);
plot(time,out5.Temp_Air_Souffle, 'r',linewidth =2);
plot(time,out6.Temp_Air_Souffle,'b',linewidth =2);
ylabel('Temperature(°C)');
xlabel('Time(hours)');
legend('Temp-Supply-Air-exist-sys-for-any-Hr','Temp-Supply-Air-with-Hr-vacant-house','Temp-Supply-Air-with-Hr-low-occupancy-house','Temp-Supply-Air-with-Hr-occupied-house');
hold off;
grid on;
subplot(2,3,3);
hold on;
plot(time,out1.Temp_Air_Exhaust,'m',linewidth =2);
plot(time,out4.Temp_Air_Exhaust,'k',linewidth =2);
plot(time,out5.Temp_Air_Exhaust,'r',linewidth =2);
plot(time,out6.Temp_Air_Exhaust,'b',linewidth =2);
ylabel('Temperature(°C)');
xlabel('Time(hours)');
legend('Temp-Exhaust-Air-exist-sys-for-any-Hr','Temp-Exhaust-Air-with-Hr-vacant-house','Temp-Exhaust-Air-with-Hr-low-occupancy-house','Temp-Exhaust-Air-with-Hr-occupied-house');
grid on;
hold off;
subplot(2,3,4);
hold on;
plot(time,out7.Temp_Air_Ext,'',linewidth =2);
plot(time,out7.Temp_Air_chauffe,'m',linewidth =2);
plot(time,out10.Temp_Air_chauffe,'k',linewidth =2);
plot(time,out11.Temp_Air_chauffe,'r',linewidth =2);
plot(time,out12.Temp_Air_chauffe,'b',linewidth =2);
ylabel('Temperature(°C)');
xlabel('Time(hours)');
legend('Temp-Fresh-Air','Temp-Heated-Air-exist-sys-for-any-Hr','Temp-Heated-Air-with-Hr-vacant-house','Temp-Heated-Air-with-Hr-low-occupancy-house','Temp-Heated-Air-with-Hr-occupied-house');
grid on;
hold off;
subplot(2,3,5);
title('Temperatures for real outdoor temperature scenario city B','FontSize',16,'FontName','Times New Roman','Color','k');
hold on;
hold on;
plot(time,out7.Temp_Air_Souffle,'m',linewidth =2);
plot(time,out10.Temp_Air_Souffle,'k',linewidth =2);
plot(time,out11.Temp_Air_Souffle, 'r',linewidth =2);
plot(time,out12.Temp_Air_Souffle,'b',linewidth =2);
ylabel('Temperature(°C)');
xlabel('Time(hours)');
legend('Temp-Supply-Air-exist-sys-for-any-Hr','Temp-Supply-Air-with-Hr-vacant-house','Temp-Supply-Air-with-Hr-low-occupancy-house','Temp-Supply-Air-with-Hr-occupied-house');
grid on;
hold off;
subplot(2,3,6);
hold on;
plot(time,out7.Temp_Air_Exhaust,'m',linewidth =2);
plot(time,out10.Temp_Air_Exhaust,'k',linewidth =2);
plot(time,out11.Temp_Air_Exhaust,'r',linewidth =2);
plot(time,out12.Temp_Air_Exhaust,'b',linewidth =2);
ylabel('Temperature(°C)');
xlabel('Time(hours)');
legend('Temp-Exhaust-Air-exist-sys-for-any-Hr','Temp-Exhaust-Air-with-Hr-vacant-house','Temp-Exhaust-Air-with-Hr-low-occupancy-house','Temp-Exhaust-Air-with-Hr-occupied-house');
grid on;
hold off;

%%%%Commande BCA
figure(2);  
sgtitle('Control BCA for real outdoor temperatures scenarios of cities A and B','FontSize',16,'FontName','Times New Roman','Color','k');
subplot(4,2,1);
plot(time,out1.Dc_BCA,'k',linewidth =2);
ylabel('Control BCA');
xlabel('Time(hours)');
legend('Cmd-BCA-exist-sys-for-any-Hr');
grid on;
subplot(4,2,2);
plot(time,out4.Dc_BCA,'m',linewidth =1.7);
ylabel('Control BCA');
xlabel('Time(hours)');
legend('Cmd-BCA-with-Hr-vacant-house');
grid on;
subplot(4,2,3);
plot(time,out5.Dc_BCA,'r',linewidth =1.7);
ylabel('Control BCA');
xlabel('Time(hours)');
legend('Cmd-BCA-with-Hr-low-occupancy-house');
grid on;
subplot(4,2,4);
plot(time,out6.Dc_BCA,'b',linewidth =1.7);
ylabel('Control BCA');
xlabel('Time(hours)');
legend('Cmd-BCA-solution-with-Hr-occupied-house');
grid on;
subplot(4,2,5);
plot(time,out7.Dc_BCA,'k',linewidth =2);
ylabel('Control BCA');
xlabel('Time(hours)');
legend('Cmd-BCA-exist-sys-for-any-Hr');
grid on;
subplot(4,2,6);
plot(time,out10.Dc_BCA,'m',linewidth =1.7);
ylabel('Control BCA');
xlabel('Time(hours)');
legend('Cmd-BCA-with-Hr-vacant-house');
grid on;
subplot(4,2,7);
plot(time,out11.Dc_BCA,'r',linewidth =1.7);
ylabel('Control BCA');
xlabel('Time(hours)');
legend('Cmd-BCA-with-Hr-low-occupancy-house');
grid on;
subplot(4,2,8);
plot(time,out12.Dc_BCA,'b',linewidth =1.7);
ylabel('Control BCA');
xlabel('Time(hours)');
legend('Cmd-BCA-solution-with-Hr-occupied-house');
grid on;

%%%%Temperature obtenues du pourcentage et point de givre
figure(3);  
subplot(2,3,1);
hold on;
plot(time,out1.Freeze_point,'k',linewidth =2);
plot(time,out1.moyenne_temp,'r',linewidth =2);
plot(time,out4.moyenne_temp,'b',linewidth =2);
ylabel('Temperature(°C)');
xlabel('Time(hours)');
legend('Dew-point','Temp-percent-Air-exist-sys-with-Hr-vacant-house','Temp-percent-Air-with-Hr-vacant-house');
hold off;
grid on;
subplot(2,3,2);
title('Temperatures for real outdoor temperature scenario city A','FontSize',18,'FontName','Helvetica','Color','k')
hold on;
plot(time,out2.Freeze_point,'k',linewidth =2);
plot(time,out2.moyenne_temp,'r',linewidth =2);
plot(time,out5.moyenne_temp,'b',linewidth =2);
legend('Dew-point','Temp-percent-Air-exist-sys-with-Hr-low-occupancy-house','Temp-percent-Air-with-Hr-low-occupancy-house');
ylabel('Temperature(°C)');
xlabel('Time(hours)');
hold off;
grid on;
subplot(2,3,3);
hold on;
plot(time,out3.Freeze_point,'k',linewidth =2);
plot(time,out3.moyenne_temp,'r',linewidth =2);
plot(time,out6.moyenne_temp,'b',linewidth =2);
legend('Dew-point','Temp-percent-Air-exist-sys-with-Hr-occupied-house','Temp-percent-Air-with-Hr-occupied-house');
ylabel('Temperature(°C)');
xlabel('Time(hours)');
hold off;
grid on;  
subplot(2,3,4);
hold on;
plot(time,out7.Freeze_point,'k',linewidth =2);
plot(time,out7.moyenne_temp,'r',linewidth =2);
plot(time,out10.moyenne_temp,'b',linewidth =2);
ylabel('Temperature(°C)');
xlabel('Time(hours)');
legend('Dew-point','Temp-percent-Air-exist-sys-with-Hr-vacant-house','Temp-percent-Air-with-Hr-vacant-house');
hold off;
grid on;
subplot(2,3,5);
hold on;
title('Temperatures for real outdoor temperature scenario city B','FontSize',18,'FontName','Helvetica','Color','k');
plot(time,out8.Freeze_point,'k',linewidth =2);
plot(time,out8.moyenne_temp,'r',linewidth =2);
plot(time,out11.moyenne_temp,'b',linewidth =2);
legend('Dew-point','Temp-percent-Air-exist-sys-with-Hr-low-occupancy-house','Temp-percent-Air-with-Hr-low-occupancy-house');
ylabel('Temperature(°C)');
xlabel('Time(hours)');
hold off;
grid on;
subplot(2,3,6);
hold on;
plot(time,out9.Freeze_point,'k',linewidth =2);
plot(time,out9.moyenne_temp,'r',linewidth =2);
plot(time,out12.moyenne_temp,'b',linewidth =2);
legend('Dew-point','Temp-percent-Air-exist-sys-with-Hr-occupied-house','Temp-percent-Air-with-Hr-occupied-house');
ylabel('Temperature(°C)');
xlabel('Time(hours)');
hold off;
grid on; 

figure(4);
hold on;
plot(time,out14.Humidite_int-10,'k',linewidth =2);
plot(time,out14.Humidite_int,'r',linewidth =2);
plot(time,out15.Humidite_int,'b',linewidth =2);
ylabel('Relative Humidity(%)');
xlabel('Time(hours)');
legend('Hr-vacant-house','Hr-low-occupancy-house','Hr-occupied-house')
grid on;
hold off;

figure(5);
subplot(2,1,1);
title('Energies for real outdoor temperature scenario city A','FontSize',13,'FontName','Times New Roman','Color','k')
hold on;
plot(time,out1.E_BCA/3600,'',linewidth =2);
plot(time,out4.E_BCA/3600,'k',linewidth =2);
plot(time,out5.E_BCA/3600,'r',linewidth =2);
plot(time,out6.E_BCA/3600,'b',linewidth =2);
ylabel('Energy absorbed(KWh)');
xlabel('Time(hours)');
legend('Energy-BCA-exist-sys-for-any-Hr','Energy-BCA-with-Hr-vacant-house','Energy-BCA-with-Hr-low-occupancy-house','Energy-BCA-with-Hr-occupied-house');
grid on;
hold off;
subplot(2,1,2);
title('Energies for real outdoor temperature scenario city B','FontSize',13,'FontName','Times New Roman','Color','k')
hold on;
plot(time,out7.E_BCA/3600,'',linewidth =2);
plot(time,out10.E_BCA/3600,'K',linewidth =2);
plot(time,out11.E_BCA/3600,'r',linewidth =2);
plot(time,out12.E_BCA/3600,'b',linewidth =2);
ylabel('Energy absorbed(KWh)');
xlabel('Time(hours)');
legend('Energy-BCA-exist-sys-for-any-Hr','Energy-BCA-with-Hr-vacant-house','Energy-BCA-with-Hr-low-occupancy-house','Energy-BCA-with-Hr-occupied-house');
grid on;
hold off;

figure(6);
sgtitle('Risk freeze or dew for real outdoor temperatures scenarios of cities A and B','FontSize',16,'FontName','Times New Roman','Color','k');
subplot(4,3,1);
plot(time,out1.Risk_givre,'b',linewidth= 2);
ylabel('State');
xlabel('Time(hours)');
legend('Risk-freeze-exist-sys-with-Hr-vacant-house');
grid on;
subplot(4,3,2);
plot(time,out2.Risk_dew,'r',linewidth= 2);
ylabel('State');
xlabel('Time(hours)');
legend('Risk-dew-exist-sys-with-Hr-low-occupancy-house');
grid on;
subplot(4,3,3);
plot(time,out3.Risk_dew,'k',linewidth= 2);
ylabel('State');
xlabel('Time(hours)');
legend('Risk-dew-exist-sys-with-Hr-occupied-house');
grid on;
subplot(4,3,4);
plot(time,out4.Risk_givre,'b',linewidth= 2);
ylabel('State');
xlabel('Time(hours)');
legend('Risk-freeze-with-Hr-vacant-house');
grid on;
subplot(4,3,5);
plot(time,out5.Risk_dew,'r',linewidth= 2);
ylabel('State');
xlabel('Time(hours)');
legend('Risk-dew-with-Hr-low-occupancy-house');
grid on;
subplot(4,3,6);
plot(time,out6.Risk_dew,'k',linewidth= 2);
ylabel('State');
xlabel('Time(hours)');
legend('Risk-dew-with-Hr-occupied-house');
grid on;
subplot(4,3,7);
plot(time,out7.Risk_givre,'b',linewidth= 2);
ylabel('State');
xlabel('Time(hours)');
legend('Risk-freeze-exist-sys-with-Hr-vacant-house');
grid on;
subplot(4,3,8);
plot(time,out8.Risk_dew,'r',linewidth= 2);
ylabel('State');
xlabel('Time(hours)');
legend('Risk-dew-exist-sys-with-Hr-low-occupancy-house');
grid on;
subplot(4,3,9);
plot(time,out9.Risk_dew,'k',linewidth= 2);
ylabel('State');
xlabel('Time(hours)');
legend('Risk-dew-exist-sys-with-Hr-occupied-house');
grid on;
subplot(4,3,10);
plot(time,out10.Risk_givre,'b',linewidth= 2);
ylabel('State');
xlabel('Time(hours)');
legend('Risk-freeze-with-Hr-vacant-house');
grid on;
subplot(4,3,11);
plot(time,out11.Risk_dew,'r',linewidth= 2);
ylabel('State');
xlabel('Time(hours)');
legend('Risk-dew-with-Hr-low-occupancy-house');
grid on;
subplot(4,3,12);
plot(time,out12.Risk_dew,'k',linewidth= 2);
ylabel('State');
xlabel('Time(hours)');
legend('Risk-dew-with-Hr-occupied-house');
grid on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%End%%%
     